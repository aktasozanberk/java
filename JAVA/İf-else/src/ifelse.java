
public class ifelse {

	public static void main(String[] args) 
	{
		int sayi = 24;
		if (sayi>20) {
			System.out.println("Sayı 20'den büyüktür");
		} 
		else if (sayi<20)
		{
			System.out.println("Sayı 20'den küçüktür");
		}
		else
		{
			System.out.println("Sayı 20'ye eşittir");
		}
		
		//
		int sayi1=25;
		int sayi2=24;
		int sayi3=23;
		int enBuyukSayi=sayi1;
		
		if (enBuyukSayi<sayi2) 
		{
			enBuyukSayi=sayi2;
		} 
		else if(enBuyukSayi<sayi3) 
		{
			enBuyukSayi=sayi3;
		}
		
		System.out.println("En büyük sayı: " + enBuyukSayi);
	}

}
