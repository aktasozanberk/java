
public class arrays {

	public static void main(String[] args) {
		int[] sayi= {1,2,3,4,5,6,7,8,9,0};
		for (int i = 0; i < sayi.length; i++) {
			System.out.println(i);
		}
		
		String [] ogrenciler= new String  [4];	
		for (int i = 0; i < ogrenciler.length; i++) 
		{
			ogrenciler[i]="Ozan";
			System.out.println(ogrenciler[i]);
		}
		
		for (String ogrencİ:ogrenciler) {
			System.out.println(ogrencİ);
		}
		
		//
		
		double [] sayilar= {1.9,1.3,1.4,1.8};
		double toplam = 0 ;
		double max=sayilar[0];
		
		for (int i = 0; i < sayilar.length; i++) 
		{
			toplam+=sayilar[i];
			if (max<sayilar[i]) 
			{
				max=sayilar[i];
			}
		}
		System.out.println("Toplam: " + toplam);
		System.out.println("Max: " + max);
	}
}
